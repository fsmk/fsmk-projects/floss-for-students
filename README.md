
# FLOSS for students


## Purpose

Popularization of FOSS among students through FOSS tools and resources and thus inspire them to adopt the FREE SOFTWARE culture

## Target Audience

Preferably for students aged between 8-16 ( This is subjected to change based on the content we are going to present)

## Duration of the Program
3-4 hours.

## Program Structure

| starts at | ends at | activity |
| ------ | ------ |------ |
| 09:30 AM | 10:00 AM |Introduction, Team Activity/Ice breaking |
| 10:00 AM | 10:20 AM |Workshop |
| 10:20 AM | 11:00 AM |Activity based on workshop |
| 11:00 AM | 11:10 AM |T Break |
| 11:10 AM | 12:00 AM |Presentation/Feedbak/Q&A |
| 10:00 AM | 10:20 AM |Workshop |

## Content of the program ?

## Resources involved ?

## How can I contribute ?

## How can I co-host this program ?


